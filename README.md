# Mega Man Maker Custom NSF Changer
Made by Timothy GFO (https://gamefeveronline.gitlab.io | https://youtube.com/gamefeveronline)

## What is this?
I got tired of waiting for actual custom music to be added to Mega Man Maker. So, I made this tool for changing the nsf files on Mega Man Maker to a custom one. The awesome part is that it works while you are playing. You only need to restart the level or respawn from check point. I made this in just one day (first commit), but I expect this to improve a lot in the future.

![Alt text](/nsf_changer.png?raw=true "NSF Changer window")

## Installation
You will need to install through PIP the package PySimpleGUI, requests, and packaging. You can just do "pip install -r requirements.txt". The rest of the dependencies *should* already be installed.

## Usage
To use it just launch the executable file (if available for your os) or type "python mmm-nsf-changer.py" in the terminal or commandline, and the GUI will launch. Then you choose your nsf file and where your Mega Man Maker folder is.

## Features
* Easy way to choose a NSF, NSFe, SPC, VGM, GYM, AY, HES, KSS, SAP or GBS file as a replacement for every song in the game. Yes, you heard that right: You can use SNES Music, Sega Genesis Music, and more in Mega Man Maker!
* A "mute" button for replacing every song in the game with a silent song. For people that play Mega Man Maker with external music, like streamers.
* Reliable way to restore the original songs.
* A decent interface.
* -NEW- Support for "permanently" replacing a specific song to one of your liking, with restoring capabilities.

## Support
If you have any issues make a gitlab issue. There may be bugs.

## Roadmap
* fix restore_ogmusic so that it doesn't replace custom music. Possible windows only behaviour, testing is needed.
* consider the posibility of adding a alternate, custom music preserving, updater for Mega Man Maker.
* improve the single-song actions to make them more persistant
* modify restore_ogmusic so it is able to restore only the last change. Maybe config files will be needed for this.
* make it so that if "you press apply music and then replace song" it checks for that and doesn't allow the "replaced song" to be reverted back to original.
* ability to preserve changes through Mega Man Maker updates, by using a config file and/or a downloaded zip of the game.
* stop using os.path in favour of pathlib
* switch to using a config file (or database) for keeping track of which tracks have been changed, that way only having one original_music folder and being able to *show* the user which song you used to replace which file.
* add a way to show the available nsf files in the selected folder, like a playlist.
* Instead of duplicating a single file a lot of times, maybe I'll implement symlink support.

## Contributing
You can help with the project by testing it and reporting issues, or by making pull requests. Windows testers are especially needed since I don't daily drive Windows.

## License
GNU General Public License v3.0
